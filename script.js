const mainHeading = document.querySelector(".masthead");
const mainPage = document.querySelector(".main");
const introduction  = document.querySelector(".intro");
const testArea = document.querySelector(".test-area");
const metaClass = document.querySelector(".meta");
const timing = document.querySelector(".timer");
const wrapper = document.querySelector(".test-wrapper");
const countWords = document.querySelector("#wordsCount h3");

const originText = document.querySelector("#origin-text p").innerHTML;
const testAreaId = document.querySelector("#test-area");
const watch  = document.querySelector("#clock");
const resetButton = document.querySelector("#reset");
 

var timer = [0, 0, 0, 0];
var interval;
var timerRunning = false;

//Add leading zero to numbers 9 or below (purely for aesthetics):
function leadingZero(time){
	if(time <=9){
		time = "0" + time;
	}
	return time;
}





//Run a standard minute/second/hundredths timer:
function runTimer(){
	var currTime = leadingZero(timer[0]) + ":" + leadingZero(timer[1]) + ":" + leadingZero(timer[2]);
	timing.innerHTML= currTime;
	timer[3]++;
	
	timer[0] = Math.floor((timer[3]/100)/60);
	timer[1] = Math.floor((timer[3]/100)-(timer[0]*60));
	timer[2] = Math.floor((timer[3]-(timer[1]*100)-(timer[0]*6000)));
	
}




//Match the text entered with the provided text on the page:
function spellCheck(){
	let textEntered = testAreaId.value;
	let textEnteredMatch = originText.substring(0, textEntered.length);
	
	if(textEntered==originText){
		clearInterval(interval);
		testAreaId.style.borderColor = "#0000FF";
	}
	else{
	if(textEntered == textEnteredMatch){

		testAreaId.style.borderColor = "#deb887";
	}
		else
			{
				testAreaId.style.borderColor = "#FF0000";
			}
	}
	
}

//start the timer: 
function start(){
	let textEnteredLength = testAreaId.value.length;
	if(textEnteredLength===0 && !timerRunning){
		timerRunning = true;
		interval = setInterval(runTimer, 10);
	}
	
	countWords.innerHTML = textEnteredLength + 1;



}


//Reset everything:
function reset(){
	 clearInterval(interval);
	interval = null;
	timer = [0, 0, 0, 0];
	timerRunning = false;
	
	testAreaId.value = "";
	countWords.innerHTML = "0";
	timing.innerHTML = "00:00:00";
	testAreaId.style.borderColor = "#deb887";
}



//Event listener for keyboard input and reset button:
testAreaId.addEventListener("keypress", start, false);
testAreaId.addEventListener("keyup", spellCheck, false);
resetButton.addEventListener("click", reset, false);


